"""Project 0: Image Manipulation with OpenCV.

In this assignment, you will implement a few basic image
manipulation tasks using the OpenCV library.

Use the unit tests is image_manipulation_test.py to guide
your implementation, adding functions as needed until all
unit tests pass.
"""

import cv2
import numpy as np


def flip_image(img, h=False, v=False):
    if not (h or v):
        return img
    # func mapping for (h & v) -> -1,  (h & ~v) -> 0, (~h & v) -> 1
    flipCode = (1 * v * 0 + 1 * h * 1) * (1 * v * -2 + 1)
    flippedImg = cv2.flip(img, flipCode)
    return flippedImg


def negate_image(img):
    return cv2.bitwise_not(img)


def swap_red_and_green(img):
    swappedImg = np.copy(img)
    cv2.mixChannels([img], [swappedImg], [0, 1, 1, 0, 2, 2])
    return swappedImg
