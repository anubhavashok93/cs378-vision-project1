#!/usr/bin/env python

import cv2
import numpy as np
import sys
sys.path.append('../../')
import pano_stitcher as ps

img1 = cv2.imread('plant1.jpg')
img2 = cv2.imread('plant2.jpg')
img3 = cv2.imread('plant3.jpg')

ref = img2

pano = ps.get_pano(ref, [img1, img2, img3])
cv2.imwrite('plant_pano.png', pano)