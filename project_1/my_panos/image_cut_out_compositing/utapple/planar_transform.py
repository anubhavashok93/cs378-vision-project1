#! /usr/bin/env python

def getMask(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)
    return mask


def paste_image(base, warped_image):
    empty = np.zeros(base.shape, dtype=base[0].dtype)
    composite_img = np.copy(base)
    warped_image_mask = getMask(warped_image)
    composite_img = cv2.add(empty, composite_img, mask=cv2.bitwise_not(warped_image_mask))
    composite_img = cv2.add(warped_image, composite_img)
    return composite_img


def get_warped_image(img, known_plane, base_shape):
    h, w = img.shape[:2]
    # PERSPECTIVE TRANSFORM
    src = np.array([(w, h), (0, h), (0, 0), (w, 0)], np.float32)
    dst = np.array(known_plane, np.float32)
    M = cv2.getPerspectiveTransform(src, dst)
    warped_img = cv2.warpPerspective(img, M, dsize=base_shape)
    return warped_img

import cv2
import numpy as np

# READ BASE IMAGE
base = cv2.imread('apple.jpg', -1)
base = cv2.cvtColor(base, cv2.COLOR_BGR2BGRA)
base_shape = (base.shape[:2][1], base.shape[:2][0])
planes = [[(500, 290), (285, 280), (285, 40), (480, 145)]]

# READ PLANE IMAGE
img = cv2.imread('uatxlogo.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)

# PERSPECTIVE TRANSFORM
composite_img = np.copy(base)
for plane in planes:
    warped_img = get_warped_image(img, plane, base_shape)
    composite_img = paste_image(composite_img, warped_img)

cv2.imwrite('composite.png', composite_img)
