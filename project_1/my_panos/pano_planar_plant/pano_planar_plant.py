#!/usr/bin/env python

import cv2
import numpy as np
import sys
sys.path.append('../../')
import pano_stitcher as ps

img1 = cv2.imread('plant_planar1.jpg')
img2 = cv2.imread('plant_planar2.jpg')
img3 = cv2.imread('plant_planar3.jpg')

ref = img2

pano = ps.get_planar_pano([img1, img2, img3])
cv2.imwrite('plant_planar_pano.png', pano)