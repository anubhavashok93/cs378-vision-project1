#!/usr/bin/env python

import cv2
import numpy as np
import sys
sys.path.append('../../')
import pano_stitcher as ps

img1 = cv2.imread('tower1.jpg')
img2 = cv2.imread('tower2.jpg')
img3 = cv2.imread('tower3.jpg')
img4 = cv2.imread('tower4.jpg')
img5 = cv2.imread('tower5.jpg')

pano = ps.get_pano(img3, [img1, img2, img3, img4, img5])
cv2.imwrite('tower_pano.png', pano)