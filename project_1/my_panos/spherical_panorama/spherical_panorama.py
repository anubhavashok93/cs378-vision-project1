#! /usr/bin/env python


import cv2
import cv2.cv as cv
import numpy as np


def convertToPolar(image):
    map = np.zeros(image.shape[:2] + (2,), dtype=image[0].dtype)
    h, w = image.shape[:2]
    max = [0, 0]
    min = [0, 0]
    # map each coordinate to inverse polar equivalent
    for i in range(h):
        for j in range(w):
            polarCoordinates = cv2.polarToCart(np.float32([i]), np.float32([j]))
            map[i, j] = [polarCoordinates[0].tolist()[0][0], polarCoordinates[1].tolist()[0][0]]
            if (map[i, j][0] < min[0]):
                min[0] = map[i, j][0]
            if (map[i, j][1] < min[1]):
                min[1] = map[i, j][1]
            if (map[i, j][0] > max[0]):
                max[0] = map[i, j][0]
            if (map[i, j][1] > max[1]):
                max[1] = map[i, j][1]
    
    polarImage = np.zeros((max[1] - min[1], max[0] - min[0], 4), dtype=image[0].dtype)
    print polarImage.shape
    for i in range(h):
        for j in range(w):
            x, y = map[i, j]
            print x, y
            existingPixel = polarImage[y, x]
            nextPixel = image[i, j]
            meanPixel = cv2.addWeighted(np.array([existingPixel]), 0.5, np.array([nextPixel]), 0.5, 0)
            polarImage[y, x] = meanPixel
    cv2.imwrite('polarImage.png', polarImage)

src = cv.LoadImage('pano_bldgs.png', 1)
dst = cv.CreateImage(cv.GetSize(src), 8, 3)

cv.LogPolar(src, dst, (src.width/2, src.height/2), 200,
            cv.CV_INTER_LINEAR +
            cv.CV_WARP_FILL_OUTLIERS +
            cv.CV_WARP_INVERSE_MAP)

cv.ShowImage("inverse log-polar", dst)
cv2.waitKey()
cv2.destroyAllWindows()

