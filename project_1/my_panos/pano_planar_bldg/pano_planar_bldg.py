#!/usr/bin/env python

import cv2
import numpy as np
import sys
sys.path.append('../../')
import pano_stitcher as ps

img1 = cv2.imread('bldg1.jpg')
img2 = cv2.imread('bldg2.jpg')

ref = img2

pano = ps.get_planar_pano([img1, img2, ])
cv2.imwrite('bldg_planar_pano.png', pano)