"""Project 1: Panorama stitching.

In this project, you'll stitch together images to form a panorama.

A shell of starter functions that already have tests is listed below.

"""

import cv2
import numpy as np
import math

sift = cv2.SIFT(nfeatures=5000, nOctaveLayers=4)
surf = cv2.SURF()
surf.extended = True
MIN_MATCH_COUNT = 10


def homography_with_mask(image_a, image_b):
    """Returns the homography mapping image_b into alignment with image_a.

    Arguments:
      image_a: A grayscale input image.
      image_b: A second input image that overlaps with image_a.

    Returns: the 3x3 perspective transformation matrix (aka homography)
             mapping points in image_b to corresponding points in image_a.
    """
    # find the keypoints and descriptors with SIFT
    base_img = image_b
    img = image_a
    base_features, base_descriptors = sift.detectAndCompute(base_img, None)
    next_features, next_descriptors = sift.detectAndCompute(img, None)

    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=3)

    # bf = cv2.BFMatcher()
    flann = cv2.FlannBasedMatcher(index_params, {})

    # matches = bf.knnMatch(next_descriptors, base_descriptors, k=2)
    matches = flann.knnMatch(next_descriptors, base_descriptors, k=2)

    matches = filter_knn2matches(matches)

    p1 = np.float32(
        [base_features[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)
    p2 = np.float32(
        [next_features[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
    # kpimg1 = cv2.drawKeypoints(image_b, kp1)
    # cv2.imshow('kpimg1', kpimg1)
    # kpimg2 = cv2.drawKeypoints(image_a, kp2)
    # cv2.imshow('kpimg2', kpimg2)

    M, mask = cv2.findHomography(p1, p2, cv2.RANSAC, 4.0)

    return M, mask


def homography(image_a, image_b):
    return homography_with_mask(image_a, image_b)[0]


def filter_knn2matches(matches, threshold=0.75):
    good = []
    for m, n in matches:
        if m.distance < threshold*n.distance:
            good.append(m)
    return good


def warp_image(image, homography):
    """Warps 'image' by 'homography'

    Arguments:
      image: a 3-channel image to be warped.
      homography: a 3x3 perspective projection matrix mapping points
                  in the frame of 'image' to a target frame.
    Returns:
      - a new 4-channel image containing the warped input, resized to contain
        the new image's bounds. Translation is offset so the image fits exactly
        within the bounds of the image. The fourth channel is an alpha channel
        which is zero anywhere that the warped input image does not map in the
        output, i.e. empty pixels.
      - an (x, y) tuple containing location of the warped image's upper-left
        corner in the target space of 'homography', which accounts for any
        offset translation component of the homography.
    """
    assert(image.shape[2] == 3)
    assert(homography.shape == (3, 3))

    h, w = image.shape[:2]
    (min_x, min_y), (max_x, max_y) = warped_bounds(image.shape[:2], homography)

    # Origin is just min cordinates
    origin = (min_x, min_y)

    # Get new size
    new_h, new_w = int(math.ceil(max_y - min_y)), int(math.ceil(max_x - min_x))

    # Warp image without translation
    move_h = np.matrix(np.identity(3), np.float32)
    move_h[0, 2] -= min_x
    move_h[1, 2] -= min_y

    scale_only_h = move_h * homography

    warped_image = cv2.warpPerspective(image, scale_only_h, (new_w, new_h))

    # Add alpha channel
    warped_image = cv2.cvtColor(warped_image, cv2.COLOR_BGR2BGRA)
    return warped_image, origin


def create_mosaic(images, origins):
    """Combine multiple images into a mosaic.

    Arguments:
      images: a list of 4-channel images to combine in the mosaic.
      origins: a list of the locations upper-left corner of each image in
               a common frame, e.g. the frame of a central image.

    Returns: a new 4-channel mosaic combining all of the input images. pixels
             in the mosaic not covered by any input image should have their
             alpha channel set to zero.
    """
    assert(len(images) > 0)
    (min_x, min_y), (max_x, max_y) = get_bounds(images, origins)
    size = (max_y - min_y, max_x - min_x, 4)
    base_img = np.zeros(size, dtype=images[0].dtype)
    final_size = (max_x - min_x, max_y - min_y)
    # base_img[:, :, 3] = [255]
    for i in range(len(images)):
        # adjust origin to frame
        org_x, org_y = origins[i]
        origin = (org_x - min_x, org_y - min_y)
        # create a new image (final size) where image is in correct position
        move_h = np.matrix(np.identity(3), np.float32)
        move_h[0, 2] += origin[0]
        move_h[1, 2] += origin[1]
        image = cv2.warpPerspective(images[i], move_h, final_size)
        base_img = blend_images(base_img, image)
        h, w = base_img.shape[:2]

    return base_img


def get_bounds(images, origins):
    minx = origins[0][0]
    miny = origins[0][1]
    maxx = origins[0][0]
    maxy = origins[0][1]

    for i in range(len(images)):
        h, w = images[i].shape[:2]
        x, y = origins[i]
        minx = min(x, minx)
        miny = min(y, miny)
        maxx = max(x + w, maxx)
        maxy = max(y + h, maxy)

    # dimensions of new image should be (minx, miny) to (maxx, maxy)
    return [(int(minx), int(miny)), (int(maxx), int(maxy))]



def get_pano(ref, images):
    """Create a panorama from a reference image and perspective images.
        
        Arguments:
        ref: a 3-channel image that is the central image
        images: a list of 3-channel images to stitch together.
        
        Returns: a new 4-channel panorama combining all of the input images.
        """
    base_image = ref
    warped_images = []
    origins = []
    while len(images) > 0:
        # find closest image
        base_image = cv2.cvtColor(base_image, cv2.COLOR_BGRA2BGR)
        image = closestImage(base_image, images)
        images = [im for im in images if not np.array_equal(image, im)]
        base_image_grayscale = cv2.cvtColor(base_image, cv2.COLOR_BGR2GRAY)
        H = homography(base_image_grayscale, image)
        H = H / H[2, 2]
        warped_image, origin = warp_image(image, H)
        base_image = cv2.cvtColor(base_image, cv2.COLOR_BGR2BGRA)
        base_image = create_mosaic([base_image, warped_image], [(0, 0), origin])
    return base_image

def get_planar_pano(images):
    """Create a planar panorama from images.
        
        Arguments:
        images: a list of 3-channel images to stitch together.
        
        Returns: a new 4-channel panorama combining all of the input images.
        """
    base_image = images[0]
    return get_pano(base_image, images)

def warped_bounds(dim_orig, homography):
    h, w = dim_orig
    pts = np.float32([(w, h), (0, h), (0, 0), (w, 0)]).reshape(-1, 1, 2)
    pts_scaled = cv2.perspectiveTransform(pts, np.float32(homography))
    # pts_scaled = np.concatenate((pts_scaled, pts), axis=0)
    minb = tuple(np.float32(pts_scaled.min(axis=0).ravel()).tolist())
    maxb = tuple(np.float32(pts_scaled.max(axis=0).ravel()).tolist())

    return [minb, maxb]


# BLENDING
def getMask(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)
    return mask


def blend_images(img1, img2):
    # create frames of equal size for both images
    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]
    size = (max(h1, h2), max(w1, w2), 4)

    empty = np.zeros(size, dtype=img1[0].dtype)

    img1_frame = np.copy(empty)
    img2_frame = np.copy(empty)

    img1_frame[:h1, :w1] = img1
    img2_frame[:h2, :w2] = img2

    # MASKS
    img1_mask = getMask(img1_frame)
    img2_mask = getMask(img2_frame)

    # find intersecting parts
    intersection_mask = cv2.bitwise_and(img1_mask, img2_mask)

    # find mask of non intersecting portions
    img1_mask_ninter = cv2.subtract(img1_mask, intersection_mask)
    img2_mask_ninter = cv2.subtract(img2_mask, intersection_mask)

    # INTERSECTIONS
    # blend intersecting parts with empty image to average each pixel
    img1_i = cv2.add(empty, img1_frame, mask=intersection_mask)
    img1_i = cv2.addWeighted(empty, 0.5, img1_i, 0.5, 0)
    img2_i = cv2.add(empty, img2_frame, mask=intersection_mask)
    img2_i = cv2.addWeighted(empty, 0.5, img2_i, 0.5, 0)

    # COMBINE IMAGE
    # first combine empty frame + img2 using non-intersecting img2_mask
    final_img = cv2.add(empty, img2_frame, mask=img2_mask_ninter)
    # then combine image with img1 using both non-intersecting masks
    final_img = cv2.add(final_img, img1_frame,
                        mask=cv2.add(img1_mask_ninter, img2_mask_ninter))
    # then add intersecting parts to image
    final_img = cv2.add(final_img, img2_i)
    final_img = cv2.add(final_img, img1_i)

    return final_img


def closestImage(base_image, images):
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    flann = cv2.FlannBasedMatcher(index_params, {})
    base_features, base_descs = sift.detectAndCompute(base_image, None)
    maxInlierRatio = None
    for image in images:
        image_features, image_descs = sift.detectAndCompute(image, None)
        matches = flann.knnMatch(image_descs, trainDescriptors=base_descs, k=2)
        matches_subset = filter_knn2matches(matches)
        kp1 = []
        kp2 = []
        for match in matches_subset:
            kp1.append(base_features[match.trainIdx])
            kp2.append(image_features[match.queryIdx])
        p1 = np.array([k.pt for k in kp1])
        p2 = np.array([k.pt for k in kp2])
        H, status = cv2.findHomography(p1, p2, cv2.RANSAC, 5.0)
        inlierRatio = float(np.sum(status)) / float(len(status))
        if inlierRatio > maxInlierRatio or not maxInlierRatio:
            maxInlierRatio = inlierRatio
            closestImage = image
    return closestImage

