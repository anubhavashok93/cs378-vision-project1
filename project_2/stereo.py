"""Project 2: Stereo vision.

In this project, you'll extract dense 3D information from stereo image pairs.
"""

import cv2
import math
import numpy as np
import StringIO
from matplotlib import pyplot as plt

ply_header = '''ply
format ascii 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''


def rectify_pair(image_left, image_right, viz=False):
    """Computes the pair's fundamental matrix and rectifying homographies.

    Arguments:
      image_left, image_right: 3-channel images making up a stereo pair.

    Returns:
      F: the fundamental matrix relating epipolar geometry between the pair.
      H_left, H_right: homographies that warp the left and right image so
        their epipolar lines are corresponding rows.
    """

    sift = cv2.SIFT()
    kp_left, des_left = sift.detectAndCompute(image_left, None)
    kp_right, des_right = sift.detectAndCompute(image_right, None)

    # BFMatcher
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(des_left, des_right, k=2)

    good = []
    pts_left = []
    pts_right = []

    for m, n in matches:
        if m.distance / n.distance < .63:
            good.append(m)
            pts_right.append(kp_right[m.trainIdx].pt)
            pts_left.append(kp_left[m.queryIdx].pt)

    pts_left = np.float32(pts_left)
    pts_right = np.float32(pts_right)

    F, mask = cv2.findFundamentalMat(pts_left, pts_right, cv2.FM_LMEDS)

    pts_left = pts_left[mask.ravel() == 1]
    pts_right = pts_right[mask.ravel() == 1]

    # Reshape keypoints to 1d array
    r, c = pts_left.shape
    assert pts_left.shape == pts_right.shape
    pts_left = np.reshape(pts_left, (r * c))
    pts_right = np.reshape(pts_right, (r * c))

    success, h_left, h_right = cv2.stereoRectifyUncalibrated(
        pts_left, pts_right, F, image_left.shape[:2])
    assert success

    # Visualization of epilines
    if viz:
        pts_left_reshaped = pts_left.reshape(-1, 1, 2)
        pts_right_reshaped = pts_right.reshape(-1, 1, 2)
        left_lines = cv2.computeCorrespondEpilines(pts_left_reshaped, 1, F)
        img1, img2 = drawlines(np.copy(image_left), image_right, left_lines,
                               pts_left_reshaped, pts_right_reshaped)

        right_lines = cv2.computeCorrespondEpilines(pts_right_reshaped, 2, F)
        img3, img4 = drawlines(np.copy(image_right), image_left, right_lines,
                               pts_right_reshaped, pts_left_reshaped)

        plt.subplot(121), plt.imshow(img1)
        plt.subplot(122), plt.imshow(img3)
        plt.show()

    return F, h_left, h_right


def drawlines(img1, img2, lines, pts1, pts2):
    ''' img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines '''
    r, c = img1.shape[:2]
    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        pt1 = pt1[0]
        pt2 = pt2[0]
        r = r[0]
        x0, y0 = map(int, [0, -r[2]/r[1]])
        x1, y1 = map(int, [c, -(r[2]+r[0]*c)/r[1]])
        cv2.line(img1, (x0, y0), (x1, y1), color)
        cv2.circle(img1, tuple(np.int32(pt1)), 5, color, -1)
        cv2.circle(img2, tuple(np.int32(pt2)), 5, color, -1)
    return img1, img2


def warp_image(image, homography):
    """Warps 'image' by 'homography'

    Arguments:
      image: a 3-channel image to be warped.
      homography: a 3x3 perspective projection matrix mapping points
                  in the frame of 'image' to a target frame.

    Returns:
      - a new 4-channel image containing the warped input, resized to contain
        the new image's bounds. Translation is offset so the image fits exactly
        within the bounds of the image. The fourth channel is an alpha channel
        which is zero anywhere that the warped input image does not map in the
        output, i.e. empty pixels.
      - an (x, y) tuple containing location of the warped image's upper-left
        corner in the target space of 'homography', which accounts for any
        offset translation component of the homography.
    """
    """[0][2] = tranlate in x
        [1][2] = translate in y
        [0][0] = scale in x
        [1][1] = scale in y
    """

    # Calcualte an (x,y) tuple with the location of the
    # warped images upper-left corner
    height, width, depth = image.shape
    homography_matrix = np.matrix(homography)

    zero_array = np.matrix([[0], [0], [1]])
    origin_points = homography_matrix * zero_array
    new_x1 = origin_points[0, 0] // origin_points[2, 0]
    new_y1 = origin_points[1, 0] // origin_points[2, 0]

    zero_array = np.matrix([[width], [0], [1]])
    origin_points = homography_matrix * zero_array
    new_x2 = origin_points[0, 0] // origin_points[2, 0]
    new_y2 = origin_points[1, 0] // origin_points[2, 0]

    zero_array = np.matrix([[0], [height], [1]])
    origin_points = homography_matrix * zero_array
    new_x3 = origin_points[0, 0] // origin_points[2, 0]
    new_y3 = origin_points[1, 0] // origin_points[2, 0]

    zero_array = np.matrix([[width], [height], [1]])
    origin_points = homography_matrix * zero_array
    new_x4 = origin_points[0, 0] // origin_points[2, 0]
    new_y4 = origin_points[1, 0] // origin_points[2, 0]

    y_max = max(new_y4, new_y3, new_y2, new_y1)
    y_min = min(new_y4, new_y3, new_y2, new_y1)
    x_max = max(new_x4, new_x3, new_x2, new_x1)
    x_min = min(new_x4, new_x3, new_x2, new_x1)

    # Make a new homography without translations
    identity = [[1, 0, -x_min], [0, 1, -y_min], [0, 0, 1]]
    Hprime = np.matrix(identity) * homography_matrix

    # warp image
    image3 = cv2.warpPerspective(image, Hprime,
                                 (int(x_max - x_min), int(y_max - y_min)))

    # Calculate alpha channel
    grayImg = cv2.cvtColor(image3, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(grayImg, 0, 255, cv2.THRESH_BINARY)
    channel1, channel2, channel3 = cv2.split(image3)
    threshImage = cv2.merge((channel1, channel2, channel3, thresh[1]))

    return image3, (x_min, y_min)


# TODO: not working properly with our own images
def disparity_map(image_left, image_right):
    """Compute the disparity images for image_left and image_right.

    Arguments:
      image_left, image_right: rectified stereo image pair.

    Returns:
      an single-channel image containing disparities in pixels,
        with respect to image_left's input pixels.
    """

    # 2 96 1, 2, 10, 100, 200, 20 30

    window_size = 7
    p1 = 8 * 3 * window_size ** 2
    p2 = 32 * 3 * window_size ** 2

    stereo = cv2.StereoSGBM(minDisparity=15,
                            numDisparities=112,
                            SADWindowSize=window_size,
                            P1=p1,
                            P2=p2,
                            uniquenessRatio=7,
                            speckleWindowSize=50,
                            speckleRange=1,
                            disp12MaxDiff=100)

    # blur images for better quality disparity map
    blur_left = cv2.GaussianBlur(image_left, (3, 3), 5)
    blur_right = cv2.GaussianBlur(image_right, (3, 3), 5)
    disparity = stereo.compute(blur_left, blur_right)

    # convert to 8 bit int and get scaling by 16
    disparity2 = cv2.convertScaleAbs(disparity, alpha=1.0/16)

    # disparity2 = cv2.blur(disparity2, (2, 2))
    return disparity2


def write_ply(f, verts, colors):
    verts = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts = np.hstack([verts, colors])
    f.write(ply_header % dict(vert_num=len(verts)))
    np.savetxt(f, verts, '%f %f %f %d %d %d')


def point_cloud(disparity_image, image_left, focal_length):
    """Create a point cloud from a disparity image and a focal length.

    Arguments:
      disparity_image: disparities in pixels.
      image_left: BGR-format left stereo image, to color the points.
      focal_length: the focal length of the stereo camera, in pixels.

    Returns:
      A string containing a PLY point cloud of the 3D locations of the
        pixels, with colors sampled from left_image. You may filter low-
        disparity pixels or noise pixels if you choose.
    """

    height, width, depth = image_left.shape
    Q = np.float32([[1, 0, 0, width / 2],
                    [0, -1, 0, height / 2],
                    [0, 0, focal_length, 0],
                    [0, 0, 0, 1]])

    points = cv2.reprojectImageTo3D(disparity_image, Q)
    colors = cv2.cvtColor(image_left, cv2.COLOR_BGR2RGB)

    output = StringIO.StringIO()
    write_ply(output, points, colors)
    contents = output.getvalue()

    return contents
