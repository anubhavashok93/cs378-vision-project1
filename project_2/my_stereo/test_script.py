import cv2
import stereo
import numpy as np
import re

# values with im_ are our own images
# the rest are files from the Middlebury datasets
FILES = {"trash": "trash",
         "Umbrella": "Umbrella",
         "table2": "im9",
         "books": "books"}

# test different files by choose the key part of the dictionary above
fileChoice = FILES["table2"]

if re.match(r'^im\d+', fileChoice):
    imgL = cv2.imread("my_stereo/images/" + fileChoice + "_0.png")
    imgR = cv2.imread("my_stereo/images/" + fileChoice + "_1.png")
else:
    imgL = cv2.imread("my_stereo/images/im0" + fileChoice + ".png")
    imgR = cv2.imread("my_stereo/images/im1" + fileChoice + ".png")

F, H1, H2 = stereo.rectify_pair(imgL, imgR)

imgLw, orgLw = stereo.warp_image(imgL, H2)
imgRw, orgRw = stereo.warp_image(imgR, H1)

min_shape = tuple(np.minimum(imgLw.shape, imgRw.shape))

# reshape images to min siz1e
h, w = min_shape[:2]
smallL = imgLw[:h, :w]
smallR = imgRw[:h, :w]

# TODO: doesn't work properly for rectified images
# use unwarped left image
disparity = stereo.disparity_map(smallL, smallR)
cv2.imwrite("my_stereo/disparities/" + fileChoice + ".png", disparity)
print "disparity saved as " + fileChoice + ".png"

# pass in warped, smaller left image
ply = stereo.point_cloud(disparity, smallL, 13)

with open("my_stereo/ply_files/" + fileChoice + ".ply", 'w') as f:
    f.write(ply)

print "point cloud saved as " + fileChoice + ".ply"
