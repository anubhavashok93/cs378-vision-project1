## Run test script
------------------
To regenerate our stereo images, just run
```
python test_script.py
```
in the terminal.

You can also change which images are tested by changing line 17 in the test_script.py file. 